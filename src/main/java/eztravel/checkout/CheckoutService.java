package eztravel.checkout;

import java.util.HashMap;

public class CheckoutService {

	public int checkout(String fruit) {
		return priceTag(fruit);
	}

	public int checkout(String fruit, int qty) {

		if ("Banana".equals(fruit)) {
			qty = qty - qty / 3;
		}
		else if ("Apple".equals(fruit)) {
			int sumprice = priceTag(fruit) * qty;
			int discountprice = (priceTag(fruit) / 2) * (qty / 2);
			return sumprice - discountprice;
		}

		return priceTag(fruit) * qty;
	}

	public int checkout(HashMap<String, Object> map) {

		int price = 0;
		for (String key : map.keySet()) {
			price += priceTag(key) * (Integer) map.get(key);
		}

		return price;
	}

	private int priceTag(String name) {
		switch (name) {
		case "Apple":
			return 20;
		case "Banana":
			return 30;
		case "Orange":
			return 50;
		case "Cherry":
			return 100;
		default:
			return 200000;
		}
	}
}
