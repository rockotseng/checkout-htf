package eztravel.checkout;

import java.util.HashMap;

public class CheckoutController {

	private CheckoutService service;
	
	public int checkoutFruitBox() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Apple", 1);
		map.put("Banana", 1);
		map.put("Orange", 2);
		map.put("Cherry", 1);
		return service.checkout(map);
	}
	
}
