package eztravel.checkout;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

/**
 * Apple $20 
 * Banana $30
 * Orange $50
 * Cherry $100
 */
public class CheckoutServiceTest {

	CheckoutService checkoutService;
	
	@Before
	public void setup() {
		checkoutService = new CheckoutService();
	}
	
	@Test
	public void testBuyOneApple() {
		assertEquals(20, checkoutService.checkout("Apple"));
	}
	
	@Test
	public void testBuyOneBanana() {
		assertEquals(30, checkoutService.checkout("Banana"));
	}
	
	@Test
	public void testBuyApples() {
		assertEquals(50, checkoutService.checkout("Apple",3));
	}
	
	/**
	 * Apple x 1, Banana x 1, Orange x 2, Cherry x 1 ($250)
	 */
	@Test
	public void testBuyFruitBox() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("Apple", 1);
		map.put("Banana", 1);
		map.put("Orange", 2);
		map.put("Cherry", 1);
		assertEquals(250, checkoutService.checkout(map));
	}
	
	/**
	 * 買3根香蕉 第3根免費
	 */
	@Test
	public void testBananOnSale() {
		assertEquals(60, checkoutService.checkout("Banana",3));
		assertEquals(120, checkoutService.checkout("Banana",5));
		assertEquals(120, checkoutService.checkout("Banana",6));
	}
	
	/**
	 * Buy 2 apples, second one get half price.
	 */
	@Test
	public void testAppleOnSale() {
		assertEquals(30, checkoutService.checkout("Apple",2));
		assertEquals(50, checkoutService.checkout("Apple",3));
		assertEquals(60, checkoutService.checkout("Apple",4));
	}
	
	

}
